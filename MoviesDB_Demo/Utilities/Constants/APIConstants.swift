//
//  APIConstants.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

enum API_Data : String {
    case ApiKey = "6ec3ac28b3ead00aec8981d4fd0d7c91"
    case LanguageEn = "en-US"
}
enum API_Parameters : String {
    case API_Key = "api_key"
    case Language = "language"
    case Query = "query"
    case Page = "page"
    case Include_adult = "include_adult"
}
enum API_URL : String {
    case BaseURL = "https://api.themoviedb.org/3/"
    case ResourcesURL = "https://image.tmdb.org/t/p/w500"
}
enum API_Path : String {
    case SearchMovies = "search/movie"
    case NowPlaying = "movie/now_playing"
    case MovieDetail = "movie"
}

