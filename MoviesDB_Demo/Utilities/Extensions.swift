//
//  UIImage.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

extension String {
    func getImageURL() -> URL? {
        let imageFullPath = API_URL.ResourcesURL.rawValue + self
        return URL(string: imageFullPath)
    }
}
