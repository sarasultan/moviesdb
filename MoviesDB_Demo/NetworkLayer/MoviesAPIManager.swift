//
//  MoviesAPIManager.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class MoviesAPIManager: BaseAPIManager  {
    init() {
        super.init(baseURL: API_URL.BaseURL.rawValue)
    }
    func searchMovies(queryString : String, page : Int, success:@escaping ([Movie])->Void , failure:@escaping (APIError)->Void){
        self.cancelAllRequests()
        let paramaters = [API_Parameters.API_Key.rawValue : API_Data.ApiKey.rawValue ,
                          API_Parameters.Language.rawValue : API_Data.LanguageEn.rawValue ,
                          API_Parameters.Query.rawValue : queryString ,
                          API_Parameters.Page.rawValue : page ,
                          API_Parameters.Include_adult.rawValue : false]  as [String : Any]
        self.get(url: API_Path.SearchMovies.rawValue, params: paramaters, headers: nil, success: { (response) in
            if let dic = response as? [String : Any] {
                guard let moviesArrOfDic = dic["results"] as? [[String:Any]] else {
                    failure(APIError())
                    return
                }
                let movies : [Movie] = Mapper<Movie>().mapArray(JSONArray: moviesArrOfDic)
                success(movies)
            }
            else{
                failure(APIError())
                return
            }
        }) { (error) in
            failure(error)
        }
    }
    func getNowMovies(page : Int, success:@escaping ([Movie])->Void , failure:@escaping (APIError)->Void){
        let paramaters = [API_Parameters.API_Key.rawValue : API_Data.ApiKey.rawValue ,
                          API_Parameters.Language.rawValue : API_Data.LanguageEn.rawValue ,
                          API_Parameters.Page.rawValue : page ]  as [String : Any]
        self.get(url: API_Path.NowPlaying.rawValue, params: paramaters, headers: nil, success: { (response) in
            if let dic = response as? [String : Any] {
                guard let moviesArrOfDic = dic["results"] as? [[String:Any]] else {
                    failure(APIError())
                    return
                }
                let movies : [Movie] = Mapper<Movie>().mapArray(JSONArray: moviesArrOfDic)
                success(movies)
            }
            else{
                failure(APIError())
                return
            }
        }) { (error) in
            failure(error)
        }
    }
    func getMovieDetail(movieID : Double, success:@escaping (Movie)->Void , failure:@escaping (APIError)->Void){
        let paramaters = [API_Parameters.API_Key.rawValue : API_Data.ApiKey.rawValue ,
                          API_Parameters.Language.rawValue : API_Data.LanguageEn.rawValue] as [String : Any]
        
        self.get(url: API_Path.MovieDetail.rawValue + "/\(movieID)", params: paramaters, headers: nil, success: { (response) in
            guard let movie : Movie = Mapper<Movie>().map(JSONObject: response as! [String : Any]) else {
                failure(APIError())
                return
            }
            success(movie)
        }) { (error) in
            failure(error)
        }
    }
}
