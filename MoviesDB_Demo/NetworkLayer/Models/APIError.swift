//
//  APIError.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class APIError: NSObject {
    var errorCode : String
    var errorMessage : String
    
    override init() {
        self.errorCode = ""
        self.errorMessage = ""
    }
    init(errCode : String , errMessage : String) {
        self.errorCode = errCode
        self.errorMessage = errMessage
    }
}
