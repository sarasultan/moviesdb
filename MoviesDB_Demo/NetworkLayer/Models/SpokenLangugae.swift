//
//  SpokenLangugae.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper

class SpokenLangugae: NSObject , Mappable{
    var iso_639_1: String?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        iso_639_1 <- map["iso_639_1"]
        name <- map["name"]
    }
}
