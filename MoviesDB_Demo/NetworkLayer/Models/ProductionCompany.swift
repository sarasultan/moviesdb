//
//  ProductionCompany.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductionCompany: NSObject , Mappable{
    var companyId: Int?
    var logo_path: String?
    var name: String?
    var origin_country: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        companyId <- map["id"]
        logo_path <- map["logo_path"]
        name <- map["name"]
        origin_country <- map["origin_country"]
    }
}
