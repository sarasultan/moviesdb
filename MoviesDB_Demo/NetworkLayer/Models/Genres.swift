//
//  Genres.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper
class Genres: NSObject , Mappable {
    var genresID: Int?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        genresID <- map["id"]
        name <- map["name"]
    }
}
