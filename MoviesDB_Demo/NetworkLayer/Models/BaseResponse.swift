//
//  BaseResponse.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/7/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseResponse: NSObject , Mappable {
    var status_code : String?
    var status_message : String?
    var success : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status_code <- map["status_code"]
        status_message <- map["status_message"]
        success <- map["success"]
    }
    
}
