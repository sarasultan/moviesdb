//
//  Movie.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper

class Movie: NSObject , Mappable {
    var vote_count : Int?
    var movieID : Double?
    var video : Bool?
    var vote_average : Float?
    var title :  String?
    var popularity : Float?
    var poster_path : String?
    var original_language : String?
    var original_title : String?
    var genre_ids : [String]?
    var backdrop_path : String?
    var adult : Bool?
    var overview : String?
    
    var belongs_to_collection : String?
    var budget : String?
    var genres : [Genres]?
    var homepage : String?
    var imdb_id : String?
    var production_companies : [ProductionCompany]?
    var production_countries : [ProductionCountry]?
    var release_date : String?
    var revenue : Double?
    var runtime : Double?
    var spoken_languages : [SpokenLangugae]?
    var status : String?
    var tagline : String?
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        vote_count <- map["vote_count"]
        movieID <- map["id"]
        video <- map["video"]
        vote_average <- map["vote_average"]
        title <- map["title"]
        popularity <- map["popularity"]
        poster_path <- map["poster_path"]
        original_language <- map["original_language"]
        original_title <- map["original_title"]
        genre_ids <- map["genre_ids"]
        backdrop_path <- map["backdrop_path"]
        adult <- map["adult"]
        overview <- map["overview"]
        belongs_to_collection <- map["belongs_to_collection"]
        budget <- map["budget"]
        genres <- map["genres"]
        homepage <- map["homepage"]
        imdb_id <- map["imdb_id"]
        production_companies <- map["production_companies"]
        production_countries <- map["production_countries"]
        release_date <- map["release_date"]
        revenue <- map["revenue"]
        runtime <- map["runtime"]
        spoken_languages <- map["spoken_languages"]
        status <- map["status"]
        tagline <- map["tagline"]
    }
}
