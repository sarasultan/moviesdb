//
//  ProductionCountry.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductionCountry: NSObject , Mappable{
    var iso_3166_1: String?
    var name: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        iso_3166_1 <- map["iso_3166_1:"]
        name <- map["name"]
    }
}
