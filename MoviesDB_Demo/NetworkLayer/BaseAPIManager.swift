//
//  BaseAPIManager.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import Alamofire

class BaseAPIManager: NSObject {
    var baseURL : String

    public init(baseURL : String) {
        self.baseURL = baseURL
        super.init()
    }
    func get(url : String, params:  Parameters? , headers: HTTPHeaders?,success:@escaping (_ response : Any) -> Void , failure: @escaping (_ error : APIError)-> Void){
        Alamofire.request(baseURL + url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            switch response.result{
            case .success(let value):
                success(value)
            case .failure(let error):
                let errorObj = error as NSError
                if errorObj.code != NSURLErrorCancelled {
                    let customError = APIError(errCode: "", errMessage: "")
                    failure(customError)
                }else{
                    print("Request is cancelled");
                }
            }
        }
    }
    public func post(url : String, params: Parameters?, headers:HTTPHeaders?,success:@escaping (_ response:Any) -> Void , failure:@escaping (_ error : APIError)-> Void) {
        Alamofire.request(baseURL + url, method: .post, parameters: params,encoding:JSONEncoding.default, headers: headers).validate().responseJSON{ response in
            switch response.result{
            case .success(let value):
                success(value)
            case .failure(let error):
                let errorObj = error as NSError
                if errorObj.code != NSURLErrorCancelled {
                    let customError = APIError(errCode: "\(errorObj.code)", errMessage: "")
                    failure(customError)
                }else{
                    print("Request is cancelled");
                }
            }
        }
    }
    public func cancelAllRequests(){
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
        }
    }
}
