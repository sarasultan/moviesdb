//
//  MoviesListViewController.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class MoviesListViewController: UIViewController {
    @IBOutlet weak var searchBar : UISearchBar!
    @IBOutlet weak var moviesTableView: UITableView!
    var viewModel : MoviesListViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        initBinding()
        configureTableView()
    }
    func initBinding()  {
        viewModel?.loadLatestMovies()
        viewModel?.displayFirstPageOfMovies = { [weak self] in
            self?.moviesTableView.reloadData()
        }
        viewModel?.displayError = { [weak self] error in
            let alertController = UIAlertController(title: "Error", message: error.errorMessage, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
            alertController.addAction(alertAction)
            self?.present(alertController, animated: true, completion: nil)
        }
        viewModel?.displayMoreMovies = { [weak self] range in
            let indexSet = NSIndexSet(indexesIn: range)
            var newIndexPaths = [IndexPath]()
            indexSet.enumerate({ (index, stop) in
                newIndexPaths.append(IndexPath(row: index, section: 0))
            })
            
            self?.moviesTableView.beginUpdates()
            self?.moviesTableView.insertRows(at: newIndexPaths, with: .right)
            self?.moviesTableView.endUpdates()
            
            self?.moviesTableView.finishInfiniteScroll()
        }
        viewModel?.openMovieDetail =  { [weak self] movieID in
            self?.performSegue(withIdentifier: "ShowDetail", sender: movieID)
        }
    }
    func configureTableView()  {
        moviesTableView.addInfiniteScroll { [weak self] (tableView) -> Void in
            // update table view
            self?.viewModel?.loadMoreMovies()
            // finish infinite scroll animation
        }
        moviesTableView.setShouldShowInfiniteScrollHandler { (tableView) -> Bool in
            return true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            let movieDetailVC = segue.destination as? MovieDetailViewController
            movieDetailVC?.viewModel = MovieDetailViewModel(movieID: sender as! Double)
        }
    }
}
extension MoviesListViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel?.didSelectMovie(at: indexPath.row)
    }
}
extension MoviesListViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel?.isSearchEnabled == true {
            return self.viewModel?.searchResultCellModels.count ?? 0
        }else{
            return self.viewModel?.moviesCellModels.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewModel?.isSearchEnabled == true {
            guard let rowModel = self.viewModel?.searchResultCellModels[indexPath.row] else {
                return UITableViewCell()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: rowModel.cellIdentifier())
            if let cell = cell as? CellConfigurable {
                cell.setup(rowModel)
            }
            return cell!
        }else{
            guard let rowModel = self.viewModel?.moviesCellModels[indexPath.row] else {
                return UITableViewCell()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: rowModel.cellIdentifier())
            if let cell = cell as? CellConfigurable {
                cell.setup(rowModel)
            }
            return cell!
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}
extension MoviesListViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 3 {
            self.viewModel?.search(searchText)
        }else if searchText.count == 0{
            self.viewModel?.removeSearchResults()
        }
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel?.cancelSearch()
        searchBar.resignFirstResponder()
    }
}
