//
//  SearchResultCell.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell , CellConfigurable {
    @IBOutlet weak var searchTitleLabel: UILabel!
    
    var viewModel : SearchResultCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setup(_ viewModel: RowViewModel) {
        guard let viewModel = viewModel as? SearchResultCellViewModel else { return }
        self.viewModel = viewModel
        self.searchTitleLabel.text = viewModel.movieTitle
    }
}
