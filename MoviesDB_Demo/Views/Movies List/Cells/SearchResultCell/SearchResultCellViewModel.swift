//
//  SearchResultCellViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class SearchResultCellViewModel: RowViewModel {
    var movieTitle : String
    
    init(movieTitle : String?) {
        self.movieTitle = movieTitle ?? ""
    }
    override func cellIdentifier() -> String {
        return "SearchResultCell"
    }
}
