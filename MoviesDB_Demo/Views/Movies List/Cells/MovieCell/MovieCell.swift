//
//  MovieCell.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/7/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCell: UITableViewCell , CellConfigurable{

    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieImgView: UIImageView!
    
    var viewModel : MovieCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(_ viewModel: RowViewModel) {
        guard let viewModel = viewModel as? MovieCellViewModel else { return }
        self.viewModel = viewModel
        self.movieTitleLabel.text = viewModel.movieTitle
        self.movieImgView.image = #imageLiteral(resourceName: "Placeholder")
        self.movieImgView?.sd_setImage(with: URL(string: API_URL.ResourcesURL.rawValue + viewModel.movieImgURL), completed: nil)
    }

}
