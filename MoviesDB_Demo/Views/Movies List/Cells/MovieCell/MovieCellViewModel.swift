//
//  MovieCellViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/7/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class MovieCellViewModel: RowViewModel {
    var movieTitle : String
    var movieImgURL : String
    
    init(movieTitle : String? , movieImgURL : String?) {
        self.movieTitle = movieTitle ?? ""
        self.movieImgURL = movieImgURL ?? ""
    }
    override func cellIdentifier() -> String {
        return "MovieCell"
    }
}
