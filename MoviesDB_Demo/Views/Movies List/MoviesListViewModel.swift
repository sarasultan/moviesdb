//
//  MoviesListViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/6/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class MoviesListViewModel: BaseViewModel {
    private var movies : [Movie]?
    private var searchResultMovies : [Movie]?

    var searchTxt : String = ""
    var isSearchEnabled : Bool = false
    
    var moviesCellModels = [MovieCellViewModel]()
    var searchResultCellModels = [SearchResultCellViewModel]()

    
    var currentPage : Int = 1
    var searchCurrentPage : Int = 1

    
    var displayFirstPageOfMovies: (() -> Void)?
    var displayMoreMovies: ((NSRange) -> Void)?
    var openMovieDetail: ((Double) -> Void)?

    
    func loadLatestMovies(){
        let apiManager = MoviesAPIManager()
        apiManager.getNowMovies(page: currentPage, success: {[weak self] (movies) in
            self?.movies = movies
            self?.didGetMoviesList(movies)
        }) {[weak self] (error) in
            self?.displayError?(error)
        }
    }
    func searchMovies(_ txt : String){
        let apiManager = MoviesAPIManager()
        apiManager.searchMovies(queryString: txt, page: searchCurrentPage, success: { [weak self] (movies) in
            self?.searchResultMovies = movies
            self?.didGetMoviesList(movies)

        }) { [weak self] (error) in
            self?.displayError?(error)
        }
    }
    
    func createViewModels(_ movies : [Movie]) {
        for movie in movies {
            if isSearchEnabled == false {
                let movieCellModel = MovieCellViewModel(movieTitle: movie.title, movieImgURL: movie.poster_path)
                moviesCellModels.append(movieCellModel)
            }else{
                let searchResultViewModel = SearchResultCellViewModel(movieTitle: movie.title)
                searchResultCellModels.append(searchResultViewModel)
            }
        }
    }
    
    func didGetMoviesList(_ movies : [Movie])  {
        self.createViewModels(movies)
        if isSearchEnabled == true{
            if searchCurrentPage > 1 {
                let range = NSRange(location: self.searchResultMovies?.count ?? 0, length: movies.count)
                self.displayMoreMovies?(range)
            }else{
                self.displayFirstPageOfMovies?()
            }
        }else{
            if currentPage > 1 {
                let range = NSRange(location: self.movies?.count ?? 0, length: movies.count)
                self.displayMoreMovies?(range)
            }else{
                self.displayFirstPageOfMovies?()
            }
        }
        
    }
    func loadMoreMovies() {
        if isSearchEnabled == true {
            searchCurrentPage += 1
            searchMovies(searchTxt)
        }else{
            currentPage += 1
            loadLatestMovies()
        }
    }
    func didSelectMovie(at index : Int) {
        if isSearchEnabled == true {
            let movieID = self.searchResultMovies?[index].movieID ?? -1
            openMovieDetail?(movieID)
        }else{
            let movieID = self.movies?[index].movieID ?? -1
            openMovieDetail?(movieID)
        }
    }
    func search(_ txt : String) {
        searchResultCellModels.removeAll()
        self.isSearchEnabled = true
        searchCurrentPage = 1
        
        searchTxt = txt
        searchMovies(txt)
    }
    func cancelSearch(){
        self.isSearchEnabled = false
        self.displayFirstPageOfMovies?()
    }
    func removeSearchResults() {
        searchResultCellModels.removeAll()
        self.displayFirstPageOfMovies?()
    }
}
