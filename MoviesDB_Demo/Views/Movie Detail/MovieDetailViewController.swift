//
//  MovieDetailViewController.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    @IBOutlet weak var movieDetailsTableView: UITableView!

    var viewModel : MovieDetailViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        initBinding()
    }
    
    func initBinding(){
        viewModel?.loadMovieDetails()
        
        viewModel?.displayError = { [weak self] error in
            let alertController = UIAlertController(title: "Error", message: error.errorMessage, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Ok", style: .destructive, handler: nil)
            alertController.addAction(alertAction)
            self?.present(alertController, animated: true, completion: nil)
        }
        
        viewModel?.displayMovieDetails = { [weak self] in
            self?.movieDetailsTableView.reloadData()
        }
    }
}
extension MovieDetailViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionTitle = self.viewModel?.sectionViewModels[section].sectionTitle
        if sectionTitle == nil || sectionTitle == "" {
            return 0
        }else{
            return 40
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionTitle = self.viewModel?.sectionViewModels[section].sectionTitle
        return sectionTitle
    }
}
extension MovieDetailViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel?.sectionViewModels.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section =  self.viewModel?.sectionViewModels[section]
        return section?.rowViewModels.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section =  self.viewModel?.sectionViewModels[indexPath.section]
        let rowViewModel =  section?.rowViewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: rowViewModel?.cellIdentifier() ?? "")
        if let cel = cell as? CellConfigurable {
            cel.setup(rowViewModel!)
        }
        return cell!
    }
}
