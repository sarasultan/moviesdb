//
//  MovieDetailViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class MovieDetailViewModel: BaseViewModel {
    var sectionViewModels : [SectionViewModel] = [SectionViewModel]()
    private var movieID : Double?
    
    var displayMovieDetails: (() -> Void)?

    
    init(movieID : Double) {
        self.movieID = movieID
    }
    func loadMovieDetails(){
        guard let id = movieID else {
            return
        }
        let apiManager = MoviesAPIManager()
        apiManager.getMovieDetail(movieID: id, success: { [weak self](movie) in
            self?.didGetMoviesDetails(movie)
        }) { [weak self](error) in
            self?.displayError?(error)
        }
    }
    func didGetMoviesDetails(_ movie : Movie)  {
        self.createViewModels(movie)
        self.displayMovieDetails?()
    }
    func createViewModels(_ movie : Movie) {
        sectionViewModels.removeAll()
        
        // Image & title
        let headerCellModel = HeaderCellViewModel(movieTitle: movie.title, movieImgURL: movie.poster_path, movieTagline: movie.tagline)
        sectionViewModels.append(SectionViewModel(sectionTitle: nil, rowViewModels: [headerCellModel]))
        
        // Overview
        let overviewCellModel = TextCellViewModel(text: movie.overview)
        sectionViewModels.append(SectionViewModel(sectionTitle: "Overview", rowViewModels: [overviewCellModel]))
        
        // Spoken Languages
        var spokenLanguages = ""
        if let movieSpokenLanguages = movie.spoken_languages {
            for spokenLanguage in movieSpokenLanguages {
                spokenLanguages.append(contentsOf: spokenLanguage.name ?? "")
                spokenLanguages.append(contentsOf: " , ")
            }
            spokenLanguages = String(spokenLanguages.dropLast(2))
        }
        let spokenLangugesCellModel = TextCellViewModel(text: spokenLanguages)
        sectionViewModels.append(SectionViewModel(sectionTitle: "Spoken Languages", rowViewModels: [spokenLangugesCellModel]))
        
        // Production Companies
        var productionCompanies = ""
        if let movieProductionCompanies = movie.production_companies {
            for productionCompany in movieProductionCompanies {
                productionCompanies.append(contentsOf: productionCompany.name ?? "")
                productionCompanies.append(contentsOf: " , ")
            }
            productionCompanies = String(productionCompanies.dropLast(2))
        }
        let productionCompaniesCellModel = TextCellViewModel(text: productionCompanies)
        sectionViewModels.append(SectionViewModel(sectionTitle: "Production Companies", rowViewModels: [productionCompaniesCellModel]))
        
        // Genres
        var genres = ""
        if let movieGenres = movie.genres {
            for gen in movieGenres {
                genres.append(contentsOf: gen.name ?? "")
                genres.append(contentsOf: " , ")
            }
            genres = String(genres.dropLast(2))
        }
        let genresCellModel = TextCellViewModel(text: genres)
        sectionViewModels.append(SectionViewModel(sectionTitle: "Genres", rowViewModels: [genresCellModel]))
        
        //ProductionCountry
        var productionCountries = ""
        if let movieProductionCountries = movie.production_countries {
            for country in movieProductionCountries {
                productionCountries.append(contentsOf: country.name ?? "")
                productionCountries.append(contentsOf: " , ")
            }
            productionCountries = String(productionCountries.dropLast(2))
        }
        let productionCountriesCellModel = TextCellViewModel(text: productionCountries)
        sectionViewModels.append(SectionViewModel(sectionTitle: "Production Countries", rowViewModels: [productionCountriesCellModel]))
        
        // Votes
        let totalVotesCellModel = TextCellViewModel(text: "Total votes : \(String(format: "%d", movie.vote_count ?? 0))")
        let voteAvrgCellModel = TextCellViewModel(text: "\(String(format: "%.1f", movie.vote_average ?? 0))/10")
        sectionViewModels.append(SectionViewModel(sectionTitle: "Votes", rowViewModels: [voteAvrgCellModel , totalVotesCellModel]))
    }
    
}
