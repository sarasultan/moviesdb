//
//  HeaderCellViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class HeaderCellViewModel: RowViewModel {
    var movieTitle : String
    var movieImgURL : String
    var movieTagline : String
    
    init(movieTitle : String? , movieImgURL : String?, movieTagline : String?) {
        self.movieTitle = movieTitle ?? ""
        self.movieImgURL = movieImgURL ?? ""
        self.movieTagline = movieTagline ?? ""
    }
    override func cellIdentifier() -> String {
        return "HeaderCell"
    }
}
