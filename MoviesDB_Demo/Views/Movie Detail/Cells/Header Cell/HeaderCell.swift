//
//  HeaderCell.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell , CellConfigurable{
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieImgView: UIImageView!
    @IBOutlet weak var movieTaglineLabel: UILabel!

    
    var viewModel : HeaderCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setup(_ viewModel: RowViewModel) {
        guard let viewModel = viewModel as? HeaderCellViewModel else { return }
        self.viewModel = viewModel
        self.movieTitleLabel.text = viewModel.movieTitle
        self.movieImgView.image = #imageLiteral(resourceName: "Placeholder")
        self.movieImgView?.sd_setImage(with: viewModel.movieImgURL.getImageURL(), completed: nil)
        self.movieTaglineLabel.text = viewModel.movieTagline
    }
}
