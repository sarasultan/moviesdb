//
//  TextCellViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class TextCellViewModel: RowViewModel {
    var text : String?
    
    init(text : String? ) {
        self.text = text ?? ""
    }
    override func cellIdentifier() -> String {
        return "TextCell"
    }
}
