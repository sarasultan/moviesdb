//
//  SectionViewModel.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

class SectionViewModel: NSObject {
    var sectionTitle : String?
    var rowViewModels : [RowViewModel] = [RowViewModel]()
    
    init(sectionTitle : String? , rowViewModels : [RowViewModel]) {
        self.sectionTitle = sectionTitle
        self.rowViewModels = rowViewModels
    }
}
