//
//  CellConfigurable.swift
//  MoviesDB_Demo
//
//  Created by Sara Sultan on 8/8/19.
//  Copyright © 2019 Sara Sultan. All rights reserved.
//

import UIKit

protocol CellConfigurable {
    func setup(_ viewModel : RowViewModel)
}
